<%--
  Created by IntelliJ IDEA.
  User: whill
  Date: 6/3/15
  Time: 8:55 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="sourcePath" value="${(param.jsSRC != 'true') ? 'dist' : 'src'}"/>
<html>
	<head>
		<title>Home</title>
		<c:choose>
			<c:when test="${sourcePath == 'src'}">
				<script type="text/javascript" src="js/amd/required_libs/require.js"></script>
			</c:when>
		</c:choose>
		<script type="text/javascript" src='js/amd/<c:out value="${sourcePath}"/>/common.js'></script>
		<script type="text/javascript">requirejs.config({baseUrl: 'js/amd/<c:out value="${sourcePath}"/>'});</script>
	</head>
	<body>
		Home Page
		<script>
			//Load common code that includes config, then load the app
			//logic for this page. Do the requirejs calls here instead of
			//a separate file so after a build there are only 2 HTTP
			//requests instead of three.
			requirejs(['app/home/main'], function(homeMain) {
				console.log('--------------------------------');
				console.log('in INDEX.jsp line 31');
				console.log(homeMain);
				console.log('In INDEX.jsp line 33');
				console.log('--------------------------------');
			});
		</script>
	</body>
</html>