/**
 * Created by whill on 6/3/15.
 * #BuiltOn
 */
 define(
	[
		'require',
		'jquery'
	],
	function(require) {

		console.log('--------------------------------');
		console.log('In CDP module');

		var
			$ = require('jquery');

		console.log($);
		console.log('--------------------------------');

		return {'home-return': 'returning from CDP module'};
	}
);