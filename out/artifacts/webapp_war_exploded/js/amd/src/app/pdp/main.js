/**
 * Created by whill on 6/3/15.
 * #BuiltOn
 */
 define(
	[
		'require',
		'jquery',
		'text',
		'commonJS'
	],
	function(require) {

		console.log('--------------------------------');
		console.log('In PDP module');

		var
			$ = require('jquery'),
			txt = require('text'),
			common = require('commonJS');
			//log = common.loggers.log;

		console.log($);
		console.log(txt);
		console.log(common);
		//console.log(log);
		//log('This is from the LOGGER');
		console.log('--------------------------------');
		return {'home-return': 'returning from PDP module'};
	}
);