/**
 * Created by whill on 6/15/15.
 * #BuiltOn
 */
define(
	[
		'require',
		'log',
		'logDetail'
	],
	function(require) {
		var
			log = require('log'),
			logDetail = require('logDetail');

		log('--------------------------------');
		console.log('In LOGGER module');
		console.log(log);
		console.log(logDetail);
		console.log('--------------------------------');

		return {
			log: log
		};
	}
)