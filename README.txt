RequireJS multi-page project, with SHIM support and r.js optimizer configured via build.js (installed globally via NPM (NodeJS
Package Manager)).

For build file configuration questions, scenarios, problems... please refer to the following resources:
1. https://github.com/jrburke/r.js/blame/0937b6c3d1a31e6cce4b4616b590e67194553e30/build/example.build.js
2. http://www.ericfeminella.com/blog/2012/03/24/preprocessing-modules-with-requirejs-optimizer/
    2b. https://groups.google.com/forum/#!topic/requirejs/uBAUDhKQQLw

Project Setup Notes:
+ Tomcat 8 is configured as part of this project. If you don't have Tomcat 8, just switch to 7 in the "Edit Configurations"
+ An external tool was created and added as the first part of the build process. The external tool refers to the r.js bin
installed via NPM. The path to the binary (bin) on your system may be different, so just update the path to it. You can find it
 by going to the command line and typing (without the quotes) "which r.js" and in my case the following is returned:
 /usr/local/bin/r.js.

 Additional Notes:
 We want to strip out console.log/dir/error/etc... as part of the optimization. Print statments should only be in the src
 version of the scripts, which is controlled via the jsSRC param (jsSRC=true means: show source). The build file configuration
 resources listed above will provide guidance on how to achieve this.