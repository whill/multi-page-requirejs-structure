/**
 * Created by whill on 6/3/15.
 * #BuiltOn
 */
requirejs.config({
	//baseUrl is set in JSP due to src or dist url param dependency
	paths: {
		commonJS: 'app/common/main',
		jquery: '../required_libs/jQuery214',
		log: '../required_libs/consolelog',
		logDetail: '../required_libs/consolelog.detailprint',
		loggers: 'app/loggers/loggers',
		maybe: 'app/monads/maybe',
		requireLib: '../required_libs/require.min',
		text: '../required_libs/text'
	},
	shim: {
		logDetail: {
			deps: ['log'],
			exports: 'window.log.detailPrint'
		}
	}
});