/**
 * Created by whill on 7/7/15.
 */
(function() {
	var
		person = {
			name: 'Win Hill',
			address: {
				street: '153 F',
				city: 'West Chester'
			}
		};

	if (person !== null && person.address !== null) {
		var
			state = person.address.state;

		if (state !== null) {
			console.log(state);
		} else {
			console.log('unknown state');
		}
	}
}());

Maybe = function(value) {
	var
		nothing = function(){return null;},
		something = function(value) {
			return function() {
				return value;
			}
		};

	if (typeof value === 'undefined' || value === null) {
		return nothing();
	}

	return something(value)();
};

//--------

function f(n) {return n * 3;}
function g(n) {return n / 2;}
//(g of f)(x) = g(f(x))
a = f(2);
b = g(a);

c = g(f(2));

//--------

Maybe = function(value) {
	Array.prototype.isNothing = function() {
		return this.length === 0;
	};

	var
		NoObject = {
			bind: function() {
				return this;
			},
			isNothing: function() {
				return true;
			},
			val: function() {
				throw new Error("Can't call val() - no value present");
			},
			maybe: function(def) {
				return def;
			}
		},
		SomeObject = function(value) {
			return {
				bind: function(fn) {
					//I'm not sure why SomeObject needs to be passed around vs a direct call to fn **Changing to direct call**.
					//SomeObject or NoObject will be returned anyway.
					//return Maybe(fn.call(this, value));
					return Maybe(fn(value));
				},
				isNothing: function() {
					return false;
				},
				val: function() {
					return value;
				},
				maybe: function(def, fn) {
					return fn(value);
				}
			}
		};

	if (typeof value === 'undefined' || value === null) {
		return NoObject;
	}

	return SomeObject(value);
};

//How is it used...
//SomeObject is returned with a bind property that is a reference to a function. This function holds the value passed
//into the Maybe MONAD and the type (loosely speaking) is set and persisted via closure. If the value passed was null or
//undefined NoObject will be returned. NoObject does have a bind property that refs a function that only returns the NoObject obj.
//Back to doing things with SomeObject... if bind is called and a function is passed to it, something may (should) occur with the
//value that was passed in at the initialization stage. Then the return statement is reached and things end with NoObject or we
//continue on with the next !undefined | !null value|object.