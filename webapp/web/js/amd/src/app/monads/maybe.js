/**
 * Created by whill on 6/15/15.
 * #BuiltOn
 */
define(
	[
		'require',
		'jquery'
	],
	function(require) {

		console.log('--------------------------------');
		console.log('In MAYBE module');

		var
			$ = require('jquery');

		/*
		This will eventually return some operation on the value "value". The function passed into the bind property will
		ultimately determine the end result of "value".

		Example:
			var t = MONAD()('test');
			var tTransformer = function(theValueToChange) {
				return theValueToChange.toUpperCase();
			};
			var tResult = t.bind(tTransformer); //Prints: TEST

		 So the t object above has the monad returned to it. In this case, the monad, through closure, holds onto the original
		 value passed in (in this case: 'test'). From this point, due to functions being first class objects and the support for
		 higher order functions (passing a function into a function as an argument), the original value can be transformed,
		 tested (maybe, identity), etc. In addition to those language level specs, the behavior of String and Number literals,
		 allow this to occur. The literals will not inherit properties like the new String and Number Objects will i.e.
		 var name = 'Win'; name.last = 'Hill'; //the extension of the name String literal (the last property) will not perist -
		 it silently fails.

		 In the example above, we convert the string 'test' to upper case -> 'TEST'

		 The monad provides a path to follow for functional operations on primitives and objects.
		 Another path would be to define functions that follow different paths, depending on the primitive or object that's
		 processed by them. Lambdas, Higher Order Functions and Functions as First class Objects along with category theory,
		 pattern matching and monads allows for very reusable real-world functional programming.
		 */
		function MONAD() {
			return function unit(value) {
				var
					monad = Object.create(null);

				monad.bind = function(func) {
					return func(value);
				};

				return monad;
			}
		}

		console.log($);
		console.log('--------------------------------');
		return {'home-return': 'returning from maybe module'};
	}
);