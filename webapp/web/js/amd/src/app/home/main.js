/**
 * Created by whill on 6/3/15.
 * #BuiltOn
 */
 define(
	[
		'require',
		'jquery',
		'text',
		'maybe'
	],
	function(require) {

		console.log('--------------------------------');
		console.log('In HOME module');

		var
			$ = require('jquery'),
			txt = require('text'),
			maybe = require('maybe');

		console.log(txt);
		console.log($);
		console.log(maybe);
		console.log('--------------------------------');
		return {'home-return': 'returning from home module'};
	}
);