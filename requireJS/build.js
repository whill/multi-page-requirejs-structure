/**
 * Created by whill on 6/3/15.
 * #BuiltOn
 */
{
    allowSourceOverwrites: false,
    appDir: '../webapp/web/js/amd/src/',
    baseUrl: '.',
    dir: '../webapp/web/js/amd/dist',
    mainConfigFile: '../webapp/web/js/amd/src/common.js',
    preserveLicenseComments: false,
    onBuildWrite: function(name, path, contents) {

        console.log('Writing ' + name);

        //contents = contents.replace(/#BuiltOn/, new Date().toString());

        return contents;
    },
    modules: [
        {
            name: 'common',
            include: [
                'commonJS',
                'jquery',
                'maybe',
                'requireLib',
                'text'
            ]
        },
        {
            name: 'app/common/main',
            exclude: ['common']
        },
        {
            name: 'app/home/main',
            exclude: ['common']
        },
        {
            name: 'app/pdp/main',
            exclude: ['common']
        }
    ]
}